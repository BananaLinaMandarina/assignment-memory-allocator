#define _DEFAULT_SOURCE
#define _GNU_SOURCE
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "test.h"
#include "util.h"

void debug(const char* fmt, ... );

static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

size_t get_blocks_count(struct block_header* block) {
    struct block_header* block_cnt = block;
    if (block_cnt == NULL) return 0;
    size_t count = 1;

    while (block_cnt->next != NULL) {
        count++;
        block_cnt = block_cnt->next;
    }

    return count;
}

void* test_init() {
    void* heap = heap_init(10000);
    if (heap == NULL) {
        err("heap not allocated");
    }
    return heap;
}

void test1(struct block_header* first_block) {
    void* chunk1 = _malloc(1000);
    if (chunk1 == NULL) err("Test 1: falied to allocate memory\n");
    if (first_block->capacity.bytes != 1000) err("Test 1: wrong capacity\n");
    if (first_block->is_free) err("Test 1: memory is free\n");
    debug("Test 1: passed\n");
    _free(chunk1);
}

void test2(struct block_header* first_block) {
    void* chunk1 = _malloc(1000);
    void* chunk2 = _malloc(1000);
    if (chunk1 == NULL || chunk2 == NULL) err("Test 2: failed to allocate memory\n");
    if (first_block->is_free) err("Test 2: block must be taken");
    _free(chunk2);
    _free(chunk1);
    if(get_blocks_count(first_block) != 1) err("Test 2: blocks are not merged\n");
    if (!first_block->is_free) err("Test 2: block is not free\n");
    debug("Test 2: passed\n");
}

void test3(struct block_header* first_block) {
    void* chunk1 = _malloc(1000);
    void* chunk2 = _malloc(1000);
    void* chunk3 = _malloc(1000);
    if (chunk1 == NULL || chunk2 == NULL || chunk3 == NULL) err("Test 3: failed to allocate memory\n");
    _free(chunk3);
    _free(chunk2);
    _free(chunk1);
    if(get_blocks_count(first_block) != 1) err("Test 3: blocks are not merged\n");
    if (!first_block->is_free) err("Test 3: block is not free\n");
    debug("Test 3: passed\n");
}

void test4(struct block_header* first_block) {
    void* chunk1 = _malloc(first_block->capacity.bytes * 2);
    if (chunk1 == NULL) err("Test 4: failed to allocate memory\n");
    _free(chunk1);
    if(get_blocks_count(first_block) != 1) err("Test 4: blocks are not merged\n");
    if (!first_block->is_free) err("Test 4: block is not free\n");
    debug("Test 4: passed\n");
}

void test5(struct block_header* first_block) {
    void* addr = (uint8_t*) first_block + first_block->capacity.bytes + 17 ;
    void* new_heap = mmap( (uint8_t*) (round_pages((size_t) addr)),
                       1000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
    void* chunk1 = _malloc(100000);
    if (chunk1 == addr) err("Test 5: block created after next block");
    _free(chunk1);
    if(get_blocks_count(first_block) != 2) err("Test 5: blocks are not merged\n");
    if (!first_block->is_free) err("Test 5: block is not free\n");
    debug("Test 5: passed\n");
    munmap(new_heap, round_pages((size_t) addr));
}

void test_malloc() {
    void* heap = test_init();
    struct block_header* first_block = (struct block_header*) (heap);
    test1(first_block);
    test2(first_block);
    test3(first_block);
    test4(first_block);
    test5(first_block);
}
